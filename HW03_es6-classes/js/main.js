class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	get name() {
		return this._name;
	}

	get age() {
		return this._age;
	}

	get salary() {
		return this._salary;
	}

	set name(newName) {
		this._name = newName;
	}

	set age(newAge) {
		this._age = newAge;
	}

	set salary(newSalary) {
		this._salary = newSalary;
	}
}

const newUser1 = new Employee("Name1", 24, 30000);
// console.log(newUser1);
// console.log(newUser1.age);
// console.log(newUser1.age = 34);
// console.log(newUser1.name);
// console.log(newUser1.name = "Name2");
// console.log(newUser1.salary);
// console.log(newUser1.salary = 45000);
// console.log(newUser1);

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}

	get salary() {
		return this._salary * 3;
	}
}

const programmer1 = new Programmer("Name3", 45, 100000, "JS");

console.table(`Programmer1 info:
    Name: ${programmer1.name}
    Age: ${programmer1.age}
    Salary: ${programmer1.salary}`);
console.log(programmer1.salary);

const programmer2 = new Programmer("Name4", 35, 200000, "Python");
const programmer3 = new Programmer("Name5", 25, 107000, "Java");
const programmer4 = new Programmer("Name6", 42, 150000, "C#");

console.log(`Programmer2 info:
            Name: ${programmer2.name}
            Age: ${programmer1.age}
            Salary: ${programmer2.salary}`);

console.log(`Programmer3 info:
            Name: ${programmer3.name}
            Age: ${programmer3.age}
            Salary: ${programmer3.salary}`);

console.log(`Programmer4 info:
            Name: ${programmer4.name}
            Age: ${programmer4.age}
            Salary: ${programmer4.salary}`);
