const btnContainer = document.querySelector(".button_container");
const button = document.querySelector(".btn");
const URL_IPIFY = "https://api.ipify.org/?format=json";
const URL_IPAPI = "http://ip-api.com/json";
const queryOptions = "status,continent,country,regionName,city,district,query";

button.addEventListener("click", getAdreesByIP);

async function getAdreesByIP() {
	const ip = await fetch(URL_IPIFY);
	const responce = await ip.json();

	const data = await fetch(
		`${URL_IPAPI}/${responce.ip}?fields=${queryOptions}`
	);
	const jsonData = await data.json();

	if (!document.querySelector(".response")) {
		renderIP(
			({
				query,
				continent,
				country,
				regionName,
				city,
				district,
			} = jsonData)
		);
	}
}

function renderIP({ continent, country, regionName, city, district }) {
	const responseDiv = document.createElement("div");
	responseDiv.classList.add("response");

	const queryP = document.createElement("p");
	const continentP = document.createElement("p");
	const countryP = document.createElement("p");
	const regionNameP = document.createElement("p");
	const cityP = document.createElement("p");
	const districtP = document.createElement("p");

	queryP.innerText = "IP: " + query;
	continentP.innerText = "Континент: " + continent;
	countryP.innerText = "Страна: " + country;
	regionNameP.innerText = "Регион: " + regionName;
	cityP.innerText = "Город: " + city;
	districtP.innerText = "Район: " + district;

	responseDiv.append(
		queryP,
		continentP,
		countryP,
		regionNameP,
		cityP,
		districtP
	);
	btnContainer.insertAdjacentElement("beforeend", responseDiv);
}
