// Дан массив городов ["Kyiv", "Berlin", "Dubai", Moscow", "Paris"];
// Выведите в консоль все свойства этого массива с помощью синтаксиса десктруктуризации.

const cities = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
const [city1, city2, city3, city4, city5] = cities;

console.log(`${city1}, ${city2}, ${city3}, ${city4}, ${city5}`);

// Создайте обьект Employee. Запишите в него свойства name, salary и присвойте любые значение.
// С помощью синтаксиса десктруктуризации сделайте так, чтоб запись console.log(name, salary)
// выводила в консоль - значения этих свойств в обьекте Employee;

const Employee = {
	name: "John",
	salary: 10000,
};

const { name, salary } = Employee;
console.log(name, salary);

// Дополните код так, чтоб он был рабочим
// const array = ['value', 'showValue']
// alert(value); // будет выведено 'value'
// alert(showValue);  // будет выведено 'showValue'

const array = ["value", "showValue"];
const [value, showValue] = array;
alert([value]); // будет выведено 'value'
alert([showValue]); // будет выведено 'showValue'
